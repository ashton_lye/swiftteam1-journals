# Swift Team 1 - Swift Journal
## Ashton & Cody
<br>

# Week 1
This week we worked through Chapter 1 of the book, finishing up with making the Light App.   
The first chapter covered the very basics of swift/programming in general - variables & constants, if statements etc which we already pretty much knew, but it's good to know how the syntax differs from C#. We also learned the basics of using xcode e.g. how to use the documentation, debugging, and the basics of the Interface Builder which blew us away - it's so simple!   
We're definitely looking forward to the more complicated stuff and so far we're loving how relaxed the syntax is - no semicolons!
<br>
<br>

# Week 2
We started work on chapter 2 of the iBook, we leant about the stuctures, classes, collections and loops and began learning about the UIKit and it's basic controls etc, and thats pretty much it.
<br>
<br>

# Week 3 
This week we continued leaning about the UIKit and controls and image views and have now started looking at the auto-layouts and stack-views. We have started working on the second chapter's app (called apple pie).
<br>
<br>

# Week 4
This week we finished the Apple Pie project and put a little of our own spin on it. We then started work on our assessment - writing the report about the best practices in Swift.

<br>
<br>

# Week 5
This week we have continued working on Assessment 1 - the report and the app. Both of us have finished our reports and had them peer reviewed. We have also both completed our apps and put them on my phone which is pretty neat. I managed to use my macOS VM for some work for it too which was cool.

<br>
<br>

# Week 6
This week we have continued working through the bok, starting chapter 3. We have learned about optionals, enums and guard statements and are currently learning how to make apps with multiple screens and transition between them.